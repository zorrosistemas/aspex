<%
'SIDEBAR START
%>    
<!--#include file="menu.inc"-->

<!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
            <a class="" href="index.asp">
                          <i class="fa fa-home fa-lg" aria-hidden="true"></i>
                          <span>Dashboard</span>
                      </a>
          </li>
          <%
          For Each base In menu
            'Response.Write(base & "<br />")
            opcoes=Split(base,",")

            if UBound(opcoes) > 0 then
              m = opcoes(1)
              o = opcoes(2)
              if opcoes(0) = 0 then 
                linkar = ""
                if opcoes(4) <> "" then 
                  linkar = "linkar('" & opcoes(4) & "')"
                end if 
              %>
          <li class="sub-menu">
            <a href="javascript:<%=linkar%>;" class="">
              <i class="fa <%=opcoes(5)%> fa-lg" aria-hidden="true"></i>
              <span><%=opcoes(3)%></span>
                          <%
                          if opcoes(4) = "" then 
                          %>
              <span class="menu-arrow arrow_carrot-right"></span>
                          <%
                          end if 
                          %>
            </a>
              <%
                if len(opcoes(4)) > 0 then
                %>
            <ul class="sub">
                <%
                conta = 0
                for each x in opcoes
                if isnumeric(x) then
                  if x > 0 then 
                    if conta = 1 then 
                      if x = m and o > 0 then 
                        linkar = "linkar('" & opcoes(4) & "')"
                %>
              <li><a class="" href="javascript:<%=linkar%>;"><%=opcoes(3)%></a></li>
                <%
                      end if 
                    end if 
                  end if 
                end if 
                conta = conta + 1
                next
                %>
            </ul>
                <%
                end if 
              end if 
              %>
          </li>
          <%
            end if 
          Next
          response.end
          %>

          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_desktop"></i>
                          <span>UI Fitures</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="general.html">Elements</a></li>
              <li><a class="" href="buttons.html">Buttons</a></li>
              <li><a class="" href="grids.html">Grids</a></li>
            </ul>
          </li>

          <li>
            <a class="" href="widgets.html">
                          <i class="icon_genius"></i>
                          <span>Widgets</span>
                      </a>
          </li>

          <li>
            <a class="" href="chart-chartjs.html">
                          <i class="icon_piechart"></i>
                          <span>Charts</span>

                      </a>

          </li>

          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_table"></i>
                          <span>Tables</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="basic_table.html">Basic Table</a></li>
            </ul>
          </li>

          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_documents_alt"></i>
                          <span>Pages</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="profile.html">Profile</a></li>
              <li><a class="" href="login.asp"><span>Login Page</span></a></li>
              <li><a class="" href="contact.html"><span>Contact Page</span></a></li>
              <li><a class="" href="blank.html">Blank Page</a></li>
              <li><a class="" href="404.html">404 Error</a></li>
            </ul>
          </li>

        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

<%
'SIDEBAR END
%>
