<%
function print_r(data, dumpRef)
  dim dumpData
  if isArray(data) or cbool(instr(TypeName(data),"Dictionary")) or TypeName(data) = "ISessionObject" then
    dumpData = dump(data, 0)
  else
    if TypeName(data) = "Recordset" then
      dumpData = dumpQuery(data)
    else
      dumpData = TypeName(data) & ": " & data
    end if
  end if 
  dumpData = "----------- DUMP CALLED " & dumpRef &" -------------<br>" & vbcrlf & dumpData
  globalDumpData = globalDumpData & vbcrlf & dumpData
  if left(dumpRef,5) = "<pre:" then
    response.write "<pre>"&dumpData&"</pre>"
    if dumpRef = "<pre:stop>" then
      response.end
    end if
  end if
  print_r = dumpData
end function
 
function dumpQuery(recordset)
  dim col, header, data, wrapper, q
  wrapper = "<table border=""1"">"&vbcrlf
  set q = recordset
  q.movefirst
  if q.absoluteposition = 1 then
    header = "  <tr>" & vbcrlf
    for each col in q.fields
      header = header & "    <th align=""left"" valign=""top"">"&col.name&"</th>" & vbcrlf
    next
    header = header & "  </tr>" & vbcrlf
    q.movefirst
  end if
 
  data =   rs.GetString(2, q.recordcount+1, "</td>" & vbcrlf & "    <td valign=""top"">", "[#]", "")
  data = left(data, len(data) - 3)
  data = replace(data, "[#]", "</td>" & vbcrlf & "  </tr>" & vbcrlf & "  <tr>" & vbcrlf & "    <td valign=""top"">")
  data = "  <tr>" & vbcrlf & "    <td valign=""top"">" & data & "</td>" & vbcrlf & "  </tr>"
  wrapper = wrapper & header & data & vbcrlf & "</table>"
  q.movefirst
  dumpQuery = wrapper
end function
 
function dump(data, depth)
  dim output, x
  if isArray(data) then
    output = "Array <br />"
    output = output & Tab(depth) & "(<br />"
    for x=0 to uBound(data)
      output = output & Tab(depth+1) & "["&x&"] => "
      output = output & dump(data(x), depth+2) 
      output = output & "<br />"
    next
    output = output & Tab(depth) & ")"
  elseif cbool(instr(TypeName(data),"Dictionary")) then
    output = TypeName(data) & " <br />"
    output = output & Tab(depth) & "(<br />"
    for each x in data
      output = output & Tab(depth+1) & "["&x&"] => "
      output = output & dump(data(x), depth+2) 
      output = output & "<br />"
    next
    output = output & Tab(depth) & ")"
  elseif TypeName(data) = "ISessionObject" then
    output = TypeName(data) & "<br />(<br/>"& Tab(depth+1) & "Contents<br />"
    output = output & Tab(depth+1) & "(<br />"
    for each x in data.contents
      output = output & Tab(depth+2) & "["&x&"] => "
      output = output & dump(data(x), depth+2) 
      output = output & "<br />"
    next
    output = output & Tab(depth+1) & ")<br/><br/>"
    output = output & Tab(depth+1) & "StaticObjects<br />"
    output = output & Tab(depth+1) & "(<br />"
    for each x in data.StaticObjects
      output = output & Tab(depth+2) & "["&x&"] => "
      output = output & dump(data(x), depth+2) 
      output = output & "<br />"
    next
    output = output & Tab(depth+1) & ")<br/>"
    output = output & Tab(depth) & ")"
  elseif TypeName(data) = "Recordset" then
    output = output & dumpQuery(data)
    output = output & "<br />"
  else
    output = output & data
  end if
  dump = output
end function
 
public function Tab(spaces)
  dim val, x
  val = ""
  for x=1 to spaces
    val = val & "    "
  next
  Tab = val
end function

public function var_dump(code)
  dim x
  x = print_r(code, "")
  response.write x
end function 

'var_dump(Session("home"))
'response.end 

%>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="aspEX Framework">
  <meta name="author" content="Zorro Sistemas ft GeeksLabs">
  <meta name="keyword" content="aspEX, asp, Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>aspEX - Default Bootstrap Admin Template</title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <!-- full calendar css-->
  <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
  <link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
  <!-- easy pie chart-->
  <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
  <!-- owl carousel -->
  <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
  <link href="css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
  <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />
  <link href="css/xcharts.min.css" rel=" stylesheet">
  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">
  <!-- =======================================================
    Theme Name: NiceAdmin
    Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <!-- container section start -->
  <section id="container" class="">


    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>

      <!--logo start-->
      <a href="index.html" class="logo">asp<span class="lite">EX</span></a>
      <!--logo end-->

      <div class="nav search-row" id="top_menu">
        <!--  search form start -->
        <ul class="nav top-menu">
          <li>
            <form class="navbar-form">
              <input class="form-control" placeholder="Procurar" type="text">
            </form>
          </li>
        </ul>
        <!--  search form end -->
      </div>

      <div class="top-nav notification-row">
        <!-- notification dropdown start-->
        <ul class="nav pull-right top-menu">

<!--#include file="notificationstasks.inc"-->
<!--#include file="notificationsinbox.inc"-->
<!--#include file="notificationsalerts.inc"-->
<!--#include file="userlogin.inc"-->

        </ul>
        <!-- notification dropdown end-->
        
      </div>
    </header>
    <!--header end-->

<!--#include file="sidebar.inc"-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
              <li><i class="fa fa-laptop"></i>Dashboard</li>
            </ol>
          </div>
        </div>

<%

if Session("home") = "" then
  Session("home") = "home" 
else 
  pagina = Session("home") 
end if 

' **** Dynamic ASP include v.2.0

function fixInclude(content)
   out=""   
   if instr(content,"#include ")>0 then
        response.write "Error: include directive not permitted!"
        response.end
   end if     
   content=replace(content,"<" & "%=" , "<" & "%response.write ")   
   pos1=instr(content,"<" & "%")
   pos2=instr(content,"%" & ">")
   if pos1>0 then
      before= mid(content,1,pos1-1)
      before=replace(before,"""","""""")
      before=replace(before,vbcrlf,""""&vbcrlf&"response.write vbcrlf&""")
      before=vbcrlf & "response.write """ & before & """" &vbcrlf
      middle= mid(content,pos1+2,(pos2-pos1-2))
      after=mid(content,pos2+2,len(content))
      out=before & middle & fixInclude(after)
   else
      content=replace(content,"""","""""")
      content=replace(content,vbcrlf,""""&vbcrlf&"response.write vbcrlf&""")
      out=vbcrlf & "response.write """ & content &""""
   end if
   fixInclude=out
end function

Function getMappedFileAsString(byVal strFilename)
  Dim fso
  Set fso = CreateObject("ADODB.Stream")

  fso.CharSet = "utf-8"
  fso.Open
  fso.LoadFromFile(Server.MapPath(strFilename))

  getMappedFileAsString = fso.ReadText()

  'Response.write(getMappedFileAsString)
  'Response.End

  Set fso = Nothing
End Function

execute (fixInclude(getMappedFileAsString(pagina & ".asp")))
%>


      </section>
      <div class="text-right">
        <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
          -->
          Powered by <a href="https://www.ecin.com.br/zorro">Zorro Sistemas</a>
        </div>
      </div>
    </section>
    <!--main content end-->
  </section>
  <!-- container section start -->

  <!-- javascripts -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
  <script src="js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
  <!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="assets/jquery-knob/js/jquery.knob.js"></script>
  <script src="js/jquery.sparkline.js" type="text/javascript"></script>
  <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
  <script src="js/owl.carousel.js"></script>
  <!-- jQuery full calendar -->
  <<script src="js/fullcalendar.min.js"></script>
    <!-- Full Google Calendar - Calendar -->
    <script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="js/calendar-custom.js"></script>
    <script src="js/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="js/jquery.customSelect.min.js"></script>
    <script src="assets/chart-master/Chart.js"></script>

    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
    <script src="js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="js/jquery-jvectormap-world-mill-en.js"></script>
    <script src="js/xcharts.min.js"></script>
    <script src="js/jquery.autosize.min.js"></script>
    <script src="js/jquery.placeholder.min.js"></script>
    <script src="js/gdp-data.js"></script>
    <script src="js/morris.min.js"></script>
    <script src="js/sparklines.js"></script>
    <script src="js/charts.js"></script>
    <script src="js/jquery.slimscroll.min.js"></script>
    <script>
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });
    </script>

</body>

</html>
